﻿#include <iostream>


void PrintNumbers(int Limit, bool IsEven) {
    std::cout << (IsEven ? "Even numbers: " : "Odd numbers: ");
    for (int i = IsEven ? 0 : 1; i <= Limit; i += 2) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
}

int main() {
    const int N = 10; 

    
    PrintNumbers(N, true);

    
    PrintNumbers(N, false);

    return 0;
}
